package com.bren.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AppGui extends JFrame {
    private static final String URL = System.getenv("URL");
    private static final String USER = System.getenv("USER_LOGIN");
    private static final String PASSWORD = System.getenv("PASS");
    private static final String SHOW_ALL_TABLE_QUERY = "select tablename from pg_catalog.pg_tables where schemaname = 'public';";

    public AppGui() {
        setSize(600, 600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JButton showTables = new JButton("Show All Table in DB");
        JButton showQuery = new JButton("Show Query Result");

        JTextField selectQueryInput = new JTextField();
        JLabel selectQueryInputLabel = new JLabel("Input your select query");

        getContentPane().add(showTables);
        getContentPane().add(selectQueryInputLabel);
        getContentPane().add(selectQueryInput);
        getContentPane().add(showQuery);
        getContentPane().setLayout(new GridLayout(18, 2));


        ActionListener showAllTables = e -> {
            getQuery(SHOW_ALL_TABLE_QUERY);
        };

        ActionListener getSelectQuery = e -> {
            String selectQuery = selectQueryInput.getText();
            if (selectQuery.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Input your query!");
            }
            getQuery(selectQuery);
        };

        showTables.addActionListener(showAllTables);
        showQuery.addActionListener(getSelectQuery);
    }

    private void getQuery(String query) {
        String[] buttons = {"Yes", "No"};
        List<String[]> dataLines = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                int rowSize = resultSet.getMetaData().getColumnCount();
                int counter = 0;
                String[] row = new String[rowSize];
                for (int i = 1; i <= rowSize; i++) {
                    row[counter] = resultSet.getString(i);
                    sb.append(resultSet.getString(i));
                    sb.append("  ");
                    counter++;
                }
                dataLines.add(row);
                sb.append("\n");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(new JFrame(), "Bad query, try again", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        JOptionPane.showMessageDialog(null, sb.toString(), "Result", JOptionPane.INFORMATION_MESSAGE);
        int choice = JOptionPane.showOptionDialog(null,
                "Do you want to save query result in CSV file?", "Question",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, buttons, buttons[1]);
        if (choice == JOptionPane.YES_OPTION) {
            try {
                saveQueryToCSVFile(dataLines);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private String convertToCSV(String[] data) {
        return String.join(",", data);
    }

    private void saveQueryToCSVFile(List<String[]> dataLines) throws IOException {
        File csvOutputFile = new File("src/main/resources/csv/queryResult.csv");
        try (PrintWriter printWriter = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(printWriter::println);
        }
    }
}
