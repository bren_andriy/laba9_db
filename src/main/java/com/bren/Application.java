package com.bren;

import com.bren.gui.AppGui;


public class Application {
    public static void main(String[] args) {
        AppGui appGui = new AppGui();
        appGui.setVisible(true);
    }
}
